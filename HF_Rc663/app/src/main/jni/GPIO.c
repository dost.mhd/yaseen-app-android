#include <fcntl.h>
#include <stdlib.h>
#include <termios.h>

#include <errno.h>
#include <string.h>
#include <dirent.h>
#include <sys/socket.h>
#include <poll.h>
#include <unistd.h>

#include <sys/ioctl.h>

#include <android/log.h>
#include <GPIO.h>

static const char *TAG="GPIO";
#define LOGI(fmt, args...) __android_log_print(ANDROID_LOG_INFO,  TAG, fmt, ##args)
#define LOGD(fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, TAG, fmt, ##args)
#define LOGE(fmt, args...) __android_log_print(ANDROID_LOG_ERROR, TAG, fmt, ##args)


//#define GPIO_SET_1D_SCAN 		   0x05
//#define GPIO_SET_RFID 			   0x03

#define GPIO_SET_1D_SCAN    _IOR('t', 1, int)
#define GPIO_SET_RFID     	_IOW('t', 2, int)


int  gpioInit(const char* devname)
{
	LOGI("gpioInit.");
	int gpio_fd;

	gpio_fd = open(devname, O_RDWR);

	if(gpio_fd < 0){
		LOGE("open_dev(devname = %s) FAIL!!!", devname);
		return -1;
	}
	LOGI("open_dev(devname = %s) success. gpio_fd = %d", devname, gpio_fd);
	return gpio_fd;
}

void  gpioExit(int fd)
{
	close(fd);
}

void  gpioSetDir(int fd, int gpio, int bit)
{
	int r = -99;
	int fq;

	if(gpio = 1)
		r = ioctl(fd, GPIO_SET_1D_SCAN, &fq);
	else
		r = ioctl(fd, GPIO_SET_RFID, &fq);
	LOGI("r = %d", r);
}

int gpioWrite (int fd, int cmd, int bit)
{
	int r = -1;
	r = write(fd, cmd, bit);
	LOGI("gpioWrite r = %d", r);
	return r;
}

int gpioRead (int fd, int cmd, int bit)
{
	int r;
	r = read(fd, cmd, bit);
	LOGI("gpioRead r = %d", r);
	return r;
}
