#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <android/log.h>
#include <GPIO.h>
#include <Serial.h>
#include <jni.h>

static const char *TAG="Scanner";
#define LOGI(fmt, args...) __android_log_print(ANDROID_LOG_INFO,  TAG, fmt, ##args)
#define LOGD(fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, TAG, fmt, ##args)
#define LOGE(fmt, args...) __android_log_print(ANDROID_LOG_ERROR, TAG, fmt, ##args)

#define GPIO_DRIVER_DEV_PATH    "/dev/GPM1"

static int _SCANNER_MOULE = -1;
static int m_series = -1;
void chksum_generate(unsigned char* pData, unsigned long lDataLength)
{
	unsigned long i;
	unsigned short sum = 0;

	for(i=0; i<lDataLength; i++)
	{
		sum += pData[i];
	}

	sum = ~sum + 1;
	pData[lDataLength] = ((unsigned char*)&sum)[1];
	pData[lDataLength+1] = ((unsigned char*)&sum)[0];
}

//过滤响应包
unsigned char  m_bufScan[256] = {0};
unsigned int   m_bufIndex = 0;
unsigned int FLAG = -1;
int isFiltAckPakat(unsigned char* readBuf, int willReadLen) {
	memcpy(m_bufScan + m_bufIndex, readBuf, willReadLen);
	m_bufIndex += willReadLen;

	int packetlen = 0;
	//过滤响应包
	while (m_bufScan[0] < 10)
	{
		if (m_bufIndex >= m_bufScan[0] + 2)
		{
			packetlen = readBuf[0] + 2;

			m_bufIndex -= packetlen;

			if (m_bufIndex <= 0) {
				m_bufIndex = 0;
				break;
			} else {
				memcpy(m_bufScan, m_bufScan + packetlen, m_bufIndex);
			}
		}
		else
		{
			return 0;
		}
	}
	return willReadLen = m_bufIndex;
}

int isCode(unsigned char* readBuf, int* readLen)
{
	int i;
	unsigned char ch;
	for (i = 0; i < *readLen; i++)
	{
		ch = readBuf[i];
		if (ch == 0x0a)
		{
			*readLen = i;
			return 1;
		}
		if ((ch >= '0' && ch <= '9')
			||(ch >= 'a' && ch <= 'z')
			||(ch >= 'A' && ch <= 'Z')
			)
		{
		}
		else
		{
			return 0;
		}
	}
	return 0;
}

int  WIrOpenSerialScan1D(const char* PortName, int Baudrate, int scanner)
{
	{
		//打开SCAN扫描，拉高GPIO口
		int gpioFd;
		gpioFd = gpioInit(GPIO_DRIVER_DEV_PATH);
		LOGI("gpioFd = %d", gpioFd);
		if(gpioFd < 0) return -1;
		char led = 0x01;
		gpioWrite(gpioFd, &led, 1);     //第三个参数1表示 1D SCAN
		gpioExit(gpioFd);
	}

	{
		_SCANNER_MOULE = scanner;   //设置扫描头设备 symbol、honeywell
	}

	if (WSlOpenPort(&m_series, PortName, Baudrate, 8, 1, 'n'))
	{
		//此部分代码是symbol模块协议代码
		if(_SCANNER_MOULE == 0)
		{
			{
				//设置扫描头模式
				unsigned char init[9] = { 0x07, 0xC6, 0x04, 0x00, 0xFF, 0x4D, 0x02, 0xFD, 0xE1 }; //{ 0x07, 0xC6, 0x04, 0x00, 0xFF, 0x8A, 0x08, 0xFD, 0x9E };
				WritePort1D(init, 9);
			}
			{
				//改变扫描返回数据格式：<data> <0x0a>
				Sleep(10);
				unsigned char init[9] = { 0x07, 0xC6, 0x04, 0x00, 0xFF, 0xEB, 0x01, 0xFD, 0x44 };
				WritePort1D(init, 9);
			}
		}
		return 1;
	}
	return 0;
}

int  WIrCloseSerialScan (void)
{
	WSlClosePort(m_series);
	Sleep(30);

	{
		//关闭SCAN读取，拉低GPIO口
		int gpioFd;
		gpioFd = gpioInit(GPIO_DRIVER_DEV_PATH);
		LOGI("gpioFd = %d", gpioFd);
		if(gpioFd < 0) return -1;
		char led = 0x01;
		gpioWrite(gpioFd, &led, 0);     //第三个参数1表示GPIO拉高  0表示拉低
		gpioExit(gpioFd);
	}

	m_series = -1;
	return 1;
}

int  WIrOpenScanner1D ()
{
	int writeLen, readLen;
	if (!WSlIsOpen(m_series))
	{
		return 0;
	}

	WSlClearComm(m_series);

	if(_SCANNER_MOULE == 0)   //symbol
	{
		unsigned char init[6] =  { 0x04, 0xE4, 0x04, 0x00 }; // { 0x07, 0xC6, 0x04, 0x08, 0x00, 0x8A, 0x04, 0xFE, 0x99 };
		unsigned char readBuf[100] = {0};
		chksum_generate(init, init[0]);

		LOGE("========Open laser========");

		writeLen = WritePort1D(init, 6);
//		m_bufIndex = 0;
		FLAG = 1;
		printLog(init, writeLen);
	}
	else if (_SCANNER_MOULE == 1)  //honeywell
	{
		unsigned char init[3] = { 0x16, 0x54, 0x0D };
		writeLen = WritePort1D(init, 3);
		if(writeLen > 0) {
			return 1;
		}
	}

	return 0;
}

int  WIrReadScanner1D (unsigned char* uReadData, unsigned char* uDataLen)
{
	unsigned char readBuf[512] = {0};
	unsigned char ack[6] =  { 0x04, 0xD0, 0x04, 0x00 };  //symbol  host ACK
	int readLen, writeAck, mLen;

	if (!WSlIsOpen(m_series))
	{
		return 0;
	}

	readLen = WSlReadPort(m_series, &readBuf, 512);

	if(readLen > 0){
		if(_SCANNER_MOULE == 0) {   		//symbol
			//过滤响应包
			if(FLAG == 1) {
				printLog(readBuf, readLen);
				int willReadLen = isFiltAckPakat(readBuf, readLen);
				printLog(m_bufScan, willReadLen);

				if(isCode(m_bufScan, &willReadLen) && willReadLen > 0) {
					uDataLen[0] = willReadLen;
					memcpy(uReadData, m_bufScan, willReadLen);
					printLog(uReadData, willReadLen);
					m_bufIndex = 0;
					FLAG = 0;

					chksum_generate(ack, ack[0]);
					writeAck = WritePort1D(ack, 6);
					printLog(ack, writeAck);

					return 1;
				}
			}
		} else if (_SCANNER_MOULE == 1) {   //honeywell
			uDataLen[0] = readLen;
			memcpy(uReadData, readBuf, readLen);
			return 1;
		}
	}
	return 0;
}

int  WIrStopScan1D ()
{
	int writeLen;
	if (WSlIsOpen(m_series))
	{
		Sleep(30);
		unsigned char init[6] = { 0x04, 0xE5, 0x04, 0x00 }; //{ 0x07, 0xC6, 0x04, 0x08, 0x00, 0x8A, 0x02, 0xFE, 0x9B };
		chksum_generate(init, init[0]);
		writeLen = WritePort1D(init, 6);
	}
	return 0;
};

int  WIrOpenSerialScan2D(const char* PortName, int Baudrate)
{
	{
		//打开SCAN扫描，拉高GPIO口
		int gpioFd;
		gpioFd = gpioInit(GPIO_DRIVER_DEV_PATH);
		LOGI("gpioFd = %d", gpioFd);
		if(gpioFd < 0) return -1;
		char led = 0x01;
		gpioWrite(gpioFd, &led, 1);     //第三个参数1表示 SCAN
		gpioExit(gpioFd);
	}

	Sleep(1000);	//设置1秒时间上电延时

	unsigned char bufLen[20] = {0};
	unsigned char readBuf[512] = {0};

	if (WSlOpenPort(&m_series, PortName, Baudrate, 8, 1, 'n'))
	{
		return 1;
	}
	return 0;
}

int  WIrOpenScanner2D()
{
	unsigned char init[3] = { 0x16, 0x54, 0x0D };
	int result;

	if (!WSlIsOpen(m_series))
	{
		return 0;
	}

	WSlClearComm(m_series);
	Sleep(30);

	result = WSlWritePort(m_series, init, 3);

	if(result > 0)
	{
		return 1;
	}
	return 0;
}

int WIrReadScanner2D(unsigned char* uReadData)
{
	unsigned char *mBufferReadData[1024] = {0};
	unsigned char *readBuf[512] = {0};
	int mBufferReadLen = 0;
	int readLen;

	if (!WSlIsOpen(m_series))
	{
		return 0;
	}

	strcpy(mBufferReadData, "");

	while(1){
		readLen = WSlReadPort(m_series, &readBuf, 512);
		Sleep(10);

		mBufferReadLen += readLen;
		strncat(mBufferReadData, readBuf, readLen);

		if(mBufferReadLen > 0 && readLen == 0){  //读取数据结束
			memcpy(uReadData, mBufferReadData, mBufferReadLen);
			return mBufferReadLen;
		}
	}
	return 0;
}


void WritePort1D(unsigned char* init, int len)
{
	//发送激活命令
	unsigned char buf1[1] =  { 0x00 };
	WSlWritePort(m_series, buf1, 1);

	Sleep(15);
	WSlWritePort(m_series, init, len);
}
