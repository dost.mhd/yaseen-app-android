#include <unistd.h>     
#include <stdio.h>
#include <fcntl.h>     
#include <termios.h>    
#include <errno.h>
#include <android/log.h>
#include <Serial.h>

static const char *TAG="Serial";
#define LOGI(fmt, args...) __android_log_print(ANDROID_LOG_INFO,  TAG, fmt, ##args)
#define LOGD(fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, TAG, fmt, ##args)
#define LOGE(fmt, args...) __android_log_print(ANDROID_LOG_ERROR, TAG, fmt, ##args)

void printLog(const char* DatBuf, int DatLen)
{
	int i;
	unsigned char temp[1024] = {0};
	for(i=0; i<DatLen; i++) {
		sprintf(temp, "%s%02x", temp, DatBuf[i]);
	}
	LOGD("DatBuf：%s DatLen: %d", temp, DatLen);
}

static int getBaudrate(int baudrate)
{
	switch(baudrate) {
		case 0: return B0;
		case 50: return B50;
		case 75: return B75;
		case 110: return B110;
		case 134: return B134;
		case 150: return B150;
		case 200: return B200;
		case 300: return B300;
		case 600: return B600;
		case 1200: return B1200;
		case 1800: return B1800;
		case 2400: return B2400;
		case 4800: return B4800;
		case 9600: return B9600;
		case 19200: return B19200;
		case 38400: return B38400;
		case 57600: return B57600;
		case 115200: return B115200;
		case 230400: return B230400;
		case 460800: return B460800;
		case 500000: return B500000;
		case 576000: return B576000;
		case 921600: return B921600;
		case 1000000: return B1000000;
		case 1152000: return B1152000;
		case 1500000: return B1500000;
		case 2000000: return B2000000;
		case 2500000: return B2500000;
		case 3000000: return B3000000;
		case 3500000: return B3500000;
		case 4000000: return B4000000;
		default: return -1;
	}
}

int set_speed(int fd, int speed)
{
	int status;
	struct termios Opt;
	tcgetattr(fd, &Opt);

	tcflush(fd, TCIOFLUSH);
	cfmakeraw(&Opt);
	cfsetispeed(&Opt, speed);
	cfsetospeed(&Opt, speed);
	status = tcsetattr(fd, TCSANOW, &Opt);
	if (status != 0)
	{
		return 0;
	}
	tcflush(fd, TCIOFLUSH);
	return 1;
}

int set_Parity(int fd, int databits, int stopbits, int parity)
{
	struct termios options;
	if (tcgetattr(fd, &options) != 0)
	{
		return 0;
	}
	options.c_cflag &= ~CSIZE;
	switch (databits)
	{
	case 7:
		options.c_cflag |= CS7;
		break;
	case 8:
		options.c_cflag |= CS8;
		break;
	default:
		return 0;
	}
	switch (parity)
	{
	case 'n':
	case 'N':
		options.c_cflag &= ~PARENB; /* Clear parity enable */
		options.c_iflag &= ~INPCK; /* Enable parity checking */
		break;
	case 'o':
	case 'O':
		options.c_cflag |= (PARODD | PARENB); 
		options.c_iflag |= INPCK; /* Disnable parity checking */
		break;
	case 'e':
	case 'E':
		options.c_cflag |= PARENB; /* Enable parity */
		options.c_cflag &= ~PARODD; 
		options.c_iflag |= INPCK; /* Disnable parity checking */
		break;
	case 'S':
	case 's': /*as no parity*/
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		break;
	default:
		return 0;
	}
	
	switch (stopbits)
	{
	case 1:
		options.c_cflag &= ~CSTOPB;
		break;
	case 2:
		options.c_cflag |= CSTOPB;
		break;
	default:
		return 0;
	}

	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG | IEXTEN); /*Input*/

	options.c_iflag &= ~(IXON | IXOFF | IXANY);
	options.c_iflag &= ~(INLCR | IGNCR | ICRNL | IUCLC | IGNBRK | BRKINT
			| PARMRK | ISTRIP);

	options.c_oflag &= ~(ONLCR | OCRNL | ONLRET | ONOCR | OLCUC | OFILL
			| CRTSCTS);
	options.c_oflag &= ~OPOST; /*Output*/

	/* Set input parity option */
	if (parity != 'n')
		options.c_iflag |= INPCK;
	tcflush(fd, TCIFLUSH);
	options.c_cc[VTIME] = 0; 
	options.c_cc[VMIN] = 0; /* Update the options and do it NOW */
	if (tcsetattr(fd, TCSANOW, &options) != 0)
	{
		return 0;
	}
	return 1;
}

void WSISetSerialGPIO(int varGpio)
{
	switch(varGpio)
	{
		case 0:
			break;
	}
}

int WSlOpenPort(int* fd, const char* PortName, int baudrate, char databits,
		char stopbits, char parity)
{
	int speed;
	LOGD("open() PortName=%s  baudrate=%d", PortName, baudrate);
	*fd = open(PortName, O_RDWR);
	LOGD("open() fd = %d", *fd);
	if (-1 == *fd)
	{
		LOGD("WSlOpenPort fail: \n");
		return 0;
	}
	else
	{
		speed = getBaudrate(baudrate);
		set_speed(*fd, speed);
		set_Parity(*fd, databits, stopbits, parity);
		return 1;
	}
}

int WSlClosePort(int fd)
{
	if (fd != -1)
	{
		close(fd);
		LOGD("close() fd = %d", fd);
	}
	return 1;
}

int WSlIsOpen(int fd)
{
	return fd != -1;
}

void WSlClearComm(int fd)
{
	tcflush(fd, TCIOFLUSH);
}

void Sleep(int millsecond)
{
	usleep(millsecond * 1000);
}

int WSlReadPort(int fd, unsigned char* DatBuf, int nMax)
{
	if (fd == -1)
	{
		return 0;
	}
	int nDatLen = 0;
	nDatLen = read(fd, DatBuf, nMax);

	if (nDatLen > 0)
	{
		return nDatLen;
	}
	return 0;
}

int WSlWritePort(int fd, unsigned char* Buf, int len)
{
	if (fd == -1)
	{
		return 0;
	}

	int ToWrite = len;
	int hasWrite = 0;
	int wPos = 0;
	while (ToWrite > 0)
	{
		hasWrite = write(fd, Buf + wPos, ToWrite);
		if (hasWrite <= 0)
			break;
		ToWrite -= hasWrite;
		wPos += hasWrite;
	}


	if (ToWrite > 0)
	{
		LOGE("WSlWritePort ERR: 写入数据失败");
	}

	return len - ToWrite;
}



