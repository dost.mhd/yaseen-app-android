/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <jni.h>
#include <android/log.h>

#include <Scanner.h>
#include <GPIO.h>
#include <Rc663.h>
#include <com_serialport_SerialportJNI.h>

static const char *TAG="com_serialport_SerialportJNI";
#define LOGI(fmt, args...) __android_log_print(ANDROID_LOG_INFO,  TAG, fmt, ##args)
#define LOGD(fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, TAG, fmt, ##args)
#define LOGE(fmt, args...) __android_log_print(ANDROID_LOG_ERROR, TAG, fmt, ##args)

static jint m_series = -1;


/*
 * Class:     com_serialport_SerialportJNI
 * Method:    WIrOpenSerialScan
 * Signature: (Ljava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_WIrOpenSerialScan1D
  (JNIEnv *env, jobject thiz, jstring portName, jint baudrate, jint scanner)
{
	const char *_portName = (*env)->GetStringUTFChars(env, portName, NULL);
	m_series = WIrOpenSerialScan1D(_portName, baudrate, scanner);
	(*env)->ReleaseStringUTFChars(env, portName, _portName);
	return m_series;
}

/*
 * Class:     com_serialport_SerialportJNI
 * Method:    WIrCloseSerialScan
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_WIrCloseSerialScan
  (JNIEnv *env, jobject thiz)
{
	return WIrCloseSerialScan();
}

/*
 * Class:     com_serialport_SerialportJNI
 * Method:    WIrOpenScanner1D
 * Signature: ([B)I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_WIrOpenScanner1D
  (JNIEnv *env, jobject thiz)
{
	return WIrOpenScanner1D();
}

/*
 * Class:     com_serialport_SerialportJNI
 * Method:    WIrReadScanner1D
 * Signature: ([B)I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_WIrReadScanner1D
  (JNIEnv *env, jobject thiz, jbyteArray uReadData, jbyteArray uDataLen)
{
	unsigned char *char_buf = (char*) ((*env)->GetByteArrayElements(env, uReadData, NULL));
	unsigned char *data_len = (char*) ((*env)->GetByteArrayElements(env, uDataLen, NULL));
	m_series = WIrReadScanner1D(char_buf, data_len);
	(*env)->ReleaseByteArrayElements(env, uReadData, char_buf, 0);
	(*env)->ReleaseByteArrayElements(env, uDataLen, data_len, 0);
	return m_series;
}

/*
 * Class:     com_serialport_SerialportJNI
 * Method:    WIrStopScan1D
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_WIrStopScan1D
  (JNIEnv *env, jobject thiz)
{
	return WIrStopScan1D();
}

/*
 * Class:     com_serialport_SerialportJNI
 * Method:    WIrOpenSerialScan2D
 * Signature: ([B)I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_WIrOpenSerialScan2D
  (JNIEnv *env, jobject thiz, jstring portName, jint baudrate)
{
	const char *_portName = (*env)->GetStringUTFChars(env, portName, NULL);
	m_series = WIrOpenSerialScan2D(_portName, baudrate);
	(*env)->ReleaseStringUTFChars(env, portName, _portName);
	return m_series;
}

/*
 * Class:     com_serialport_SerialportJNI
 * Method:    WIrStartConnScanner2D
 * Signature: ([B)I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_WIrOpenScanner2D
  (JNIEnv *env, jobject thiz)
{
	return WIrOpenScanner2D();
}

/*
 * Class:     com_serialport_SerialportJNI
 * Method:    WIrReadScanner2D
 * Signature: ([B[B)I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_WIrReadScanner2D
  (JNIEnv *env, jobject thiz, jbyteArray uReadData)
{
	int dataLen;
	unsigned char *char_buf = (char*) ((*env)->GetByteArrayElements(env, uReadData, NULL));
	dataLen = WIrReadScanner2D(char_buf);
	(*env)->ReleaseByteArrayElements(env, uReadData, char_buf, 0);
	return dataLen;
}


/*
 * Class:     com_serialport_SerialportJNI
 * Method:    Rc663_HFConnect
 * Signature: (Ljava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_Rc663_1HFConnect
  (JNIEnv *env, jobject thiz, jstring portName, jint baudrate)
{
	const char *_portName = (*env)->GetStringUTFChars(env, portName, NULL);
	m_series = Rc663_HFConnect(_portName, baudrate);
	(*env)->ReleaseStringUTFChars(env, portName, _portName);
	return m_series;
}

/*
 * Class:     com_serialport_SerialportJNI
 * Method:    Rc663_HFDisconnect
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_Rc663_1HFDisconnect
  (JNIEnv *env, jobject thiz)
{
	return Rc663_HFDisconnect();
}

/*
 * Class:     com_serialport_SerialportJNI
 * Method:    Rc663_HFInit
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_Rc663_1HFInit
  (JNIEnv *env, jobject thiz, jint protocol)
{
	return Rc663_HFInit(protocol);
}

/*
 * Class:     com_serialport_SerialportJNI
 * Method:    Rc663_HFGetUid
 * Signature: ([BB)I
 */
JNIEXPORT jint JNICALL Java_com_serialport_SerialportJNI_Rc663_1HFGetUid
  (JNIEnv *env, jobject thiz, jbyteArray uReadData, jbyteArray uDataLen)
{
	unsigned char *char_buf = (char*) ((*env)->GetByteArrayElements(env, uReadData, NULL));
	unsigned char *data_len = (char*) ((*env)->GetByteArrayElements(env, uDataLen, NULL));
	m_series = Rc663_HFGetUid(char_buf, data_len);
	(*env)->ReleaseByteArrayElements(env, uReadData, char_buf, 0);
	(*env)->ReleaseByteArrayElements(env, uDataLen, data_len, 0);
	return m_series;
}

