package com.serialport;

import com.jiangt.hf.rc663.GPSActivity;
import com.jiangt.hf.rc663.HF_RC663Activity;
import com.jiangt.hf.rc663.R;
import com.jiangt.hf.rc663.Scan2DActivity;

/**
 * 功能模块所包含的组件，可动态添加
 * @author jiangtao
 * @date 2013-01-25
 */
public enum Module
{
    desk, scan, manage, charge, tools;   //配置分级菜单

    public int[] titleId;

    public int total;

    public int[] iconId;
    
    public int src;

    public Object[] action;
    
    private Module()
    {
        switch (ordinal())
        {
            case 0:
	            total = 3;
	            iconId = 
	            	new int[] 
	            	{ 
	            		R.drawable.ic_launcher_rfid, 
	            		R.drawable.ic_launcher_2dscan, 
	            		R.drawable.ic_launcher_gps,
	            	};
	            
	            titleId =
	                new int[]
	                {
	            		R.string.HF,
	                    R.string.Scan2D, 
	                    R.string.GPS,
	                };
	            action =
	                new Object[]
	                {
	            		HF_RC663Activity.class, 
	                    Scan2DActivity.class, 
	                    GPSActivity.class,
	                };
                break;
            default:
            break;
        }
    }
}