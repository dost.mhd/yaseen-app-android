package com.jiangt.hf.rc663;

import com.serialport.Utilities;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

/**
 * GPS信息获取测试Activity
 * @author jt
 *
 */
public class GPSActivity extends SuperEcallActivity implements OnClickListener {
	
	private static final int MESSAGE_WHAT_REFRESH_NMEA = 0x99;
	private static final int MESSAGE_WHAT_REFRESH_TIPS = 0x98;
	
	private EditText mGpsTips, mGpsTime, mGpsLongitude, mGpsLatitude, mGpsAltitude;
	private TextView mGpsRead;
	
	private Criteria criteria;
	private LocationManager locationManager;
	private String provider = null;
	
	private ReadGPSThread readGPSThread;
	
	private static boolean FALG = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.gps);
        initComp();
    }
    
    @Override
	protected void initComp() {
		// TODO Auto-generated method stub
    	setTitle(getString(R.string.GPS));
    	mGpsTips = (EditText) findViewById(R.id.mGpsTips);
    	mGpsTime = (EditText) findViewById(R.id.mGpsTime);
    	mGpsLongitude = (EditText) findViewById(R.id.mGpsLongitude);
    	mGpsLatitude = (EditText) findViewById(R.id.mGpsLatitude);
    	mGpsAltitude = (EditText) findViewById(R.id.mGpsAltitude);
    	mGpsRead = (TextView) findViewById(R.id.startReadGps);
    	
    	mGpsRead.setOnClickListener(this);
    	
    	hiddenEdittextSoftinput(mGpsTips, mGpsTime, mGpsLongitude, mGpsLatitude, mGpsAltitude);
    	mGpsRead.setEnabled(false);
    	mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				openGPSSettings();
			}
		}, 2*1000L);
	}
    
    private void openGPSSettings() {
    	locationManager =(LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
	    if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
	    	showToast(R.string.gps_turn_on);
		    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		    startActivity(myIntent);
	    }
	    locationManager_init();
    }
    
    protected void hiddenEdittextSoftinput(EditText... ets) {
		for(EditText et : ets) {
			InputMethodManager manager = (InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE);
			manager.hideSoftInputFromInputMethod(et.getWindowToken(), 0);
			et.setCursorVisible(false);      
			et.setFocusable(false);         
			et.setFocusableInTouchMode(false); 
		}
	}
    
    //locationManager初始化
    protected void locationManager_init(){
    	// 查找到服务信息
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE); // 高精度
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW); // 低功耗
        
        mGpsRead.setEnabled(true);
        
        provider = locationManager.getBestProvider(criteria, true); //获取GPS信息
        Log.i(TAG, "GPS provider=" + provider);
        
        if(null != provider) {
	        Location location = locationManager.getLastKnownLocation(provider); //通过GPS获取位置
	        updateToNewLocation(location);
	        
	        //设置监听器，自动更新的最小时间为间隔N秒(1秒为1*1000，这样写主要为了方便)或最小位移变化超过N米
	    	locationManager.requestLocationUpdates(provider, 100 * 1000, 500, locationListener);
        }
    }
    
    
    LocationListener locationListener = new LocationListener(){
		//位置变化时触发
		public void onLocationChanged(Location location) {
			updateToNewLocation(location);
		}
		
		//gps禁用时触发
		public void onProviderDisabled(String provider) {
			updateLocationTips("禁用");
		}
		
		//gps开启时触发
		public void onProviderEnabled(String provider) {
			updateLocationTips("开启");
		}
		
		//gps状态变化时触发
		public void onStatusChanged(String provider, int status,Bundle extras) {
			if(status == LocationProvider.AVAILABLE){
				updateLocationTips("可见的");
			}else if(status==LocationProvider.OUT_OF_SERVICE){
				updateLocationTips("服务区外");
			}else if(status==LocationProvider.TEMPORARILY_UNAVAILABLE){
				updateLocationTips("暂停服务");
			}
		}
	};
    
	private Handler mHandler = new Handler(){
    	@Override
    	public void handleMessage(Message msg) {
    		switch (msg.what) {
				case MESSAGE_WHAT_REFRESH_NMEA:
					mGpsTips.setText("获取地理位置信息成功");
					mGpsTime.setText(Utilities.getStandardDateFromUTC(((Location) msg.obj).getTime()));
					mGpsLongitude.setText(String.valueOf(((Location) msg.obj).getLongitude()));
					mGpsLatitude.setText(String.valueOf(((Location) msg.obj).getLatitude()));
					mGpsAltitude.setText(String.valueOf(((Location) msg.obj).getAltitude()));
					dismissDialog();
					FALG = true;
					break;
				case MESSAGE_WHAT_REFRESH_TIPS:
					mGpsTips.setText((CharSequence) msg.obj);
					dismissDialog();
					FALG = true;
					break;
			}
    	};
    };
    
    private void updateToNewLocation(Location location) {
    	if(location != null){
    		Message msg = new Message();
	    	msg.what = MESSAGE_WHAT_REFRESH_NMEA;
	    	msg.obj = location;
	    	mHandler.sendMessageDelayed(msg, 500);
    	} else {
//    		updateLocationTips("无法获取地理位置信息");
    	}
    }
    
    private void updateLocationTips(String tips) {
    	Message msg = new Message();
    	msg.what = MESSAGE_WHAT_REFRESH_TIPS;
    	msg.obj = tips;
    	mHandler.sendMessage(msg);
    }
    
    @Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.startReadGps) {
			FALG = false;
			showProgressDialog(R.string.waiting);
			readGPSThread = new ReadGPSThread(criteria);
			readGPSThread.start();
		}
	}
    
    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	if(null != readGPSThread && readGPSThread.isAlive()) {
    		FALG = true;
    		readGPSThread.interrupt();
    		readGPSThread = null;
    		dismissDialog();
    	}
    	super.onBackPressed();
    }
    
    
    class ReadGPSThread extends Thread {
    	Criteria mCriteria;
    	public ReadGPSThread(Criteria criteria) {
			// TODO Auto-generated constructor stub
    		mCriteria = criteria;
		}
    	
		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(!FALG) {
				try {
					sleep(50);
					locationManager_init();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
    }


    
}
