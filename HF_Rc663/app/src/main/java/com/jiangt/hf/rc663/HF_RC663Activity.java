package com.jiangt.hf.rc663;

import com.serialport.AppContext;
import com.serialport.Utilities;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

public class HF_RC663Activity extends SuperEcallActivity implements OnClickListener {
	
	private TextView mInit, mTagRead, mInitMsg;
	private EditText mTag;
	private MCustomizeSpinnerView mCustomizeSpinner;
	
	private static String[] mSpinnerEntity;
	
	private byte[] uDataLen = new byte[2];
	private byte[] GetHFData = new byte[100];
	
	private static final int HANDLER_INIT_REFRESH = 0x99;
	private static final int HANDLER_READ_REFRESH = 0x98;
	private static final int HANDLER_OPEN_ACTIVITY = 0x97;
	
	private HFInitThread mInitThread;
	private HFReadThread mReadThread;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.hf_rc663);
        initComp();
    }
    
	@Override
	protected void initComp() {
		// TODO Auto-generated method stub
		setTitle(getString(R.string.HF));
		
		mSpinnerEntity = getResources().getStringArray(R.array.hf_rc663_spinner_entity);
		
		mTagRead = (TextView) findViewById(R.id.mReadCard);
		mInit = (TextView) findViewById(R.id.mDeviceInit);
		mInitMsg = (TextView) findViewById(R.id.m_init_msg);
		mTag = (EditText) findViewById(R.id.m_hf_tagId);
		
		mCustomizeSpinner = (MCustomizeSpinnerView) findViewById(R.id.spinner_btn);
		mCustomizeSpinner.setmDropdownItem(mSpinnerEntity);
		mCustomizeSpinner.setText(mSpinnerEntity[0]);
		mCustomizeSpinner.setFocusable(true);
		mCustomizeSpinner.setSelected(true);
		mCustomizeSpinner.setPressed(true);
		
		mInit.setEnabled(true);
		mInit.setFocusable(true);
		mTagRead.setEnabled(false);
		mTagRead.setTextColor(Color.GRAY);
		
		mInit.setOnClickListener(this);
		mTagRead.setOnClickListener(this);
		
		sendRequest(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				showProgressDialog(R.string.waiting_openModule, false);
				openSerialConn();
			}
		});
	}
	
	protected void openSerialConn() {
		int callback = AppContext.getJni().Rc663_HFConnect(AppContext.SERIALPORTPATH_TTYSAC3, AppContext.SERIALPORTSPEED_B115200);
		Message message = new Message();
		message.what = HANDLER_OPEN_ACTIVITY;
		message.obj = callback;
		message.setTarget(mhandler);
		mhandler.sendMessage(message);
	}
	
	//rc663模块初始�?
	class HFInitThread extends Thread {
		
		private int _protocol;
		
		public HFInitThread(int protocol) {
			// TODO Auto-generated constructor stub
			_protocol = protocol;
			mInitMsg.setTextColor(Color.BLUE);
			mInitMsg.setText(R.string.hf_rc663_initing);
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			int callback = AppContext.getJni().Rc663_HFInit(_protocol);
			Message message = new Message();
			message.obj = callback;
			message.what = HANDLER_INIT_REFRESH;
			mhandler.sendMessage(message);
		}
	}
	
	//rc663模块读取
	class HFReadThread extends Thread {
		
		public HFReadThread() {
			// TODO Auto-generated constructor stub
			mInitMsg.setTextColor(Color.BLUE);
			mInitMsg.setText(R.string.hf_rc663_reading);
			
			mTagRead.setEnabled(false);
			mTagRead.setTextColor(Color.GRAY);
			
			mTag.setText("");
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			int callback = AppContext.getJni().Rc663_HFGetUid(GetHFData, uDataLen);
			
			String m_uid = Utilities.byte2HexStr(GetHFData, uDataLen[0]);
			Log.i(TAG, "uid = " + m_uid);
			
			Message message = new Message();
			message.what = HANDLER_READ_REFRESH;
			message.obj = m_uid;
			message.arg1 = callback;
			message.arg2 = uDataLen[0];
			mhandler.sendMessage(message);
		}
	}
	
	Handler mhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			switch(msg.what) {
				case HANDLER_OPEN_ACTIVITY:
					if((Integer) msg.obj != 1) {
						dismissDialog();
						showDialog(R.string.error_device_open, FINISH, null);
					} else {
						dismissDialog();
						showToast(R.string.module_open_success);
					}
					break;
				case HANDLER_INIT_REFRESH:
					interruptCurrentThread();
					int result = (Integer) msg.obj;
					if(result == 1) {
						mInitMsg.setTextColor(Color.RED);
						mInitMsg.setText(R.string.hf_rc663_initmsg_success);
						mTagRead.setEnabled(true);
						mTagRead.setFocusable(true);
						mTagRead.setTextColor(Color.BLACK);
					} else {
						mInitMsg.setTextColor(Color.RED);
						mInitMsg.setText(R.string.hf_rc663_initmsg_failure);
					}
					break;
				case HANDLER_READ_REFRESH:
					interruptCurrentThread();
					boolean rc = (msg.arg1 == 1) && (msg.arg2 > 0);
					if(rc) {
						playSoundRes();
						mInitMsg.setTextColor(Color.RED);
						mInitMsg.setText(R.string.hf_rc663_readmsg_success);
						mTag.setText((String) msg.obj);
					} else {
						mInitMsg.setTextColor(Color.RED);
						mInitMsg.setText(R.string.hf_rc663_readmsg_failure);
					}
					mTagRead.setEnabled(true);
					mTagRead.setTextColor(Color.BLACK);
					break;
			}
		}
	};
	
	void interruptCurrentThread() {
		if(null != mInitThread && !mInitThread.isInterrupted()) {
			mInitThread.interrupt();
			mInitThread = null;
		}
		if(null != mReadThread && !mReadThread.isInterrupted()) {
			mReadThread.interrupt();
			mReadThread = null;
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		AppContext.getJni().Rc663_HFDisconnect();
		super.onBackPressed();
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if(view.getId() == R.id.mDeviceInit) {
			//初始�?
			mInitThread = new HFInitThread(Utilities.getIndexFromList(mSpinnerEntity, (String) mCustomizeSpinner.getText()));
			mInitThread.start();
		} else if (view.getId() == R.id.mReadCard) {
			mReadThread = new HFReadThread();
			mReadThread.start();
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub		
		if((keyCode == KeyEvent.KEYCODE_HANDLE_SHORTCUT || keyCode == KeyEvent.KEYCODE_HANDLE_SHORTCUT1) 
				&& event.getRepeatCount() == 0) {
			if(mReadThread != null && !mReadThread.isInterrupted()) {
				return false;
			}
			event.startTracking();
			mReadThread = new HFReadThread();
			mReadThread.start();
			return true;
		}	
		return super.onKeyDown(keyCode, event);
	}

}
